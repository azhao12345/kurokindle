package azhao.com.kurokindle;
import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;

import java.lang.reflect.Method;
import java.util.List;

import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodReplacement;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class Hooker implements IXposedHookLoadPackage {
    private static Tokenizer tokenizer = Tokenizer.builder().build();
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals("com.amazon.kindle"))
            return;

        findAndHookMethod("com.amazon.kcp.reader.ui.dictionary.BaseDictionaryDocument", lpparam.classLoader, "lookupDefinition", String.class, String.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                if (param.getResult() == null) {
                    XposedBridge.log("original lookup was null!");
                    List<Token> result = tokenizer.tokenize((String)param.args[0]);

                    XposedBridge.log("found base form" + result.get(0).getBaseForm());
                    // call the original thing
                    Object dictionary = param.thisObject;
                    Method originalMethod = (Method)param.method;
                    Object modifiedLookup = XposedBridge.invokeOriginalMethod(originalMethod, dictionary, new Object[] { result.get(0).getBaseForm(), param.args[1] });
                    param.setResult(modifiedLookup);
                }
            }
        });
    }
}
